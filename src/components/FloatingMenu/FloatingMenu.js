import React from "react";
import FloatingMenuItem from "./FloatingMenuItem";
import {
  DiNodejs
} from "react-icons/di";

class FloatingMenu extends React.Component {
  constructor() {
    super();

    this.state = {
      toggled: false,
    };
  }

  toggleMenu() {
    this.setState({ toggled: !this.state.toggled });
  }

  render() {
    let buttons = [];
    let className = "floating-menu";

    if (this.state.toggled) {
      className += " open";
      buttons.push(
        <FloatingMenuItem label="Item 1" icon={<DiNodejs />} action="" key="i1" />
      );
      buttons.push(
        <FloatingMenuItem label="Item 2" icon={<DiNodejs />} action="" key="i2" />
      );
    }

    buttons.push(
      <FloatingMenuItem
        label=""
        icon={<DiNodejs />}
        action={this.toggleMenu.bind(this)}
        key="m"
      />
    );

    return (
      <div className="container">
        <div className={className}>{buttons}</div>
      </div>
    );
  }
}

export default FloatingMenu;
