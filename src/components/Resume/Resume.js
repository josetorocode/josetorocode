import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Particle from "../Particle";
import Resumecontent from "./ResumeContent";
import Button from "react-bootstrap/Button";
import { AiOutlineDownload } from "react-icons/ai";
function Resume() {
  return (
    <Container fluid className="resume-section">
      <Particle />
      <Container>
        <Row className="resume">
          <Col md={12} className="resume-left">
            <h3 className="resume-title">Experience</h3>
            <Resumecontent
              title="Full Stack Developer Senior - Ripley"
              date="Septiembre 2020 - Actualidad"
              content={[
                "Desarrollo de Marketplace",
                "Desarrrollo de micro servicios con node js.",
                "Firebase",
                "Google Cloud Plataform",
                "Microfront con webpack",
                "React",
                "Locust",
                "Jest + SuperTest"
              ]}
            />
            <Resumecontent
              title="Senior Frontend Developer - Altiuz"
              date="Febrero 2018 - Junio 2020"
              content={[
                "Desarrollo Minsal Chile",
                "Desarrollo de sistema de tazacion fiscal SII Chile",
                "Desarrollo Claro Chile",
                "Desarrollo Solventa Chile",
               ]}
            />
            <Resumecontent
              title="Senior Frontend Developer - BBVA"
              date="Abril 2017 - Febrero 2018"
              content={[
                "Participa en los proyectos internos del banco BBVA con la metodología Scrum",
                "Desarrollo de arquitecturas de proyectos para el banco BBVA",
                "Desarrolla software en proyecto PES del banco bbva, participa en integracion de devOps tools en proyecto de escritorio simplificado del banco bbva."
              ]}
            />
            <Resumecontent
              title="Senior Frontend Developer - Indra"
              date="Enero 2017 - Abril 2017"
              content={[
                "Desarrollo de software y arquitectura",
                "Desarrollo de software con arquitectura Angular JS y JEE para el banco bbva proyectos (SOLICITUDES, MARGENES, GARANTÍAS)."
              ]}
            />
            <Resumecontent
              title="Ingeniero de software de Java EE - Everis"
              date="Marzo 2010 - Diciembre 2016"
              content={[
                  "Desarrollo en telefonica chile (J2EE)",
                  "Desarrollo en Claro chile (J2EE)",
                  "Desarrollo en Banco Sabadell Espania",
                  "Desarrollo en EhCS Clinic (J2EE)",
                  "QA Tester en proyectos de telefonica Chile"
              ]}
            />

          </Col>
          <Col md={12} className="resume-left">
            <h3 className="resume-title">Education</h3>
            <Resumecontent
              title="Technical Developer"
              date="2009 - 2011"
              content={["Cursa Carrera de Tecnico en desarrollo de software"]}
            />
            <Resumecontent
              title="Software Engineer"
              date="2011 - 2013"
              content={["Cursa carrera de ingeniero de software"]}
            />
          </Col>

          <Col md={12} className="resume-left">
            <h3 className="resume-title">Language</h3>
            <Resumecontent
              title="English (Basic)"
              date=""
              content={[]}
            />
            <Resumecontent
              title="Spanish - Latinoamerican"
              date=""
              content={[]}
            />
          </Col>
          
          
          
        </Row>
        <Row style={{ justifyContent: "center", position: "relative" }}>
          <Button variant="primary" target="_blank">
            <AiOutlineDownload />
            &nbsp;Download CV
          </Button>
        </Row>
      </Container>
    </Container>
  );
}

export default Resume;
