import React, { useState, useEffect } from "react";
import Preloader from "../src/components/Pre";
import Navbar from "./components/Navbar";
import Home from "./components/Home/Home";
import Footer from "./components/Footer";
import Links from "./components/Links/Links";
import About from "./components/About/About";
import Resume from "./components/Resume/Resume";
import Projects from "./components/Projects/Projects";
import history from "./@history/";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./style.css";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Dropdown from "react-bootstrap/Dropdown";

import ScrollToTop from "./components/ScrollToTop";
import { Container, Button } from "react-floating-action-button";

import { BsQuestionCircleFill } from "react-icons/bs";

import { CgMenu, CgGames } from "react-icons/cg";
import { MdLanguage } from "react-icons/md";


function App() {
  const [load, upadateLoad] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      upadateLoad(false);
    }, 1200);

    return () => clearTimeout(timer);
  }, []);

  return (
    <Router path="/" history={history}>
      <Preloader load={load} />
      <div className="App" id={load ? "no-scroll" : "scroll"}>
        <Navbar />
        <ScrollToTop />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/links" component={Links} />
          <Route path="/about" component={About} />
          <Route path="/resume" component={Resume} />
          <Route path="/projects" component={Projects} />
        </Switch>
        <Footer />
      </div>
      <Container fluid>
        <Button tooltip="Hobbies">
          <BsQuestionCircleFill />
        </Button>
        <Button tooltip="Gamer" onClick={() => alert("Games")}>
          <CgGames />
        </Button>
        <Button tooltip="Menu" rotate={true}>
          <CgMenu />
        </Button>
      </Container>
    </Router>
  );
}

export default App;
